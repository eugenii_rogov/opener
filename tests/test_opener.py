import pytest
import os
from opener import saver


@pytest.fixture()
def prepared_files_for_test():
    open('test.txt', 'w')
    open('test.json', 'w')
    return ['test.txt', 'test.json', 'none.json']


def test_saver(prepared_files_for_test):
    for file in prepared_files_for_test:
        saver(file)
        if os.path.exists(file):
            os.remove(file)


