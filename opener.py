import json
import os
from tkinter import messagebox, filedialog


class SaverManager:
    def __init__(self, file_path):
        self.file_path = file_path
        self.data = None

    def __enter__(self):
        _, extension = os.path.splitext(self.file_path)
        if self.file_path:
            if extension:
                try:
                    with open(self.file_path, 'r', encoding='utf-8') as reader:
                        self.data = reader.read()
                except FileNotFoundError:
                    messagebox.showerror(title='Error', message='File not found')
            else:
                messagebox.showerror(title='Error', message='File without .json extension')
        return self.data

    def __exit__(self, exc_type, exc_val, exc_tb):
        if self.data:
            messagebox.showinfo(title='Success', message='Conversion successfully complete!')


def saver(file_path=None):
    if not file_path:
        file_path = filedialog.askopenfilename()
    with SaverManager(file_path) as data:
        if data:
            with open(file_path, 'w', encoding='utf-8') as file:
                print(json.dumps(json.loads(data), indent=4, ensure_ascii=False), file=file)


