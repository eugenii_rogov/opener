from gui import OpenerGUI
from tkinter import Tk


def calc_win_position(w, h):
    ws = root.winfo_screenwidth()
    hs = root.winfo_screenheight()
    x = (ws / 2) - (w / 2)
    y = (hs / 2) - (h / 2)
    return x, y


if __name__ == '__main__':
    root = Tk()

    w, h = 300, 100
    x, y = calc_win_position(w, h)

    root.geometry('%dx%d+%d+%d' % (w, h, x, y))
    my_gui = OpenerGUI(root)
    root.mainloop()