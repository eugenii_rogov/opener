from tkinter import Label, Button
from opener import saver


class OpenerGUI:
    def __init__(self, master):
        self.master = master
        master.title('Opener')

        self.label = Label(master, text='This is Opener GUI')
        self.label.pack()

        self.saver_button = Button(master, text='Convert', command=saver)
        self.saver_button.pack()

        self.close_button = Button(master, text='Close', command=master.quit)
        self.close_button.pack()
